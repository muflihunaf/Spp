<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data table</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }} ">
    <link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.min.css') }} ">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }} ">
    >
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped" id="siswa-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nisn</th>
                            <th>Nama</th>
                            <th>Kelas</th>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $no = 1;
                        @endphp
                        @foreach ($siswa as $list)
                            <tr>
                                <td> {{ $no++ }} </td>
                                <td> {{ $list->nisn }} </td>
                                <td> {{ $list->nama }} </td>
                                <td> {{ $list->kelas }} </td>
                                <td>
                                        <a href=" {{ route('siswa.edit', $list->id) }} " class="btn btn-primary">Edit</a>
                                        <a href=" {{ route('siswa.destroy', $list->id ) }} " class="btn btn-danger">Hapus</a>
                                    </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
<script src=" {{ asset('js/jquery.min.js') }} "></script>
<script src=" {{ asset('js/bootstrap.min.js') }} "></script>
<script src=" {{ asset('js/jquery.dataTables.min.js') }} "></script>
<script src=" {{ asset('js/dataTables.bootstrap.min.js') }} "></script>

<script>
    $(function() {
    $('#siswa-table').DataTable()
            })
</script>
</html>
