<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Datatable</title>
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/datatables/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <script src="{{ asset('assets/sweetalert2/sweetalert2.min.js') }}"></script>
    <link href="{{ asset('assets/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/bootstrap/css/ie10-viewport-bug-workaround.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/bootstrap/css/navbar-fixed-top.css') }}" rel="stylesheet">
    <script src="{{ asset('assets/bootstrap/js/ie-emulation-modes-warning.js') }}"></script>
</head>

    
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Siswa
                            <a onclick="addform()" class="btn btn-primary pull-right" style="margin-top: -8px;">Tambah</a>
                        </h4>
                    </div>
                    <div class="panel panel-body">
                        <table id="contact-table" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th >No</th>
                                        <th>Nisn</th>
                                        <th>Nama</th>
                                        <th>kelas</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                        </table>
                        
                    </div>
                </div>
            </div>
        </div>
        @include('siswa/tambah')
    </div>
</body>
        <script src="{{ asset('assets/jquery/jquery-1.12.4.min.js') }}"></script>
        <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/dataTables/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/dataTables/js/dataTables.bootstrap.min.js') }}"></script>
        <script type="text/javascript">
        var table =  $('#contact-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('api.siswa') }}",
            columns: [
                {data: 'id_siswa', name: 'id_siswa'},
                {data: 'nisn', name: 'nisn'},
                {data: 'nama', name: 'nama'},
                {data: 'kelas', name: 'kelas'},
                
            ]
        });
        function addform() {
            save_method ="add";
            $('input[name=_method]').val('POST');
            $('#modal-form').modal('show');
            $('#modal-form form')[0].reset();
            $('.modal-title').text('Add Siswa');
        }

        $(function() {
            $('#modal-form form').validator().on('submit', function(e) {
               if (!e.isDefaultPrevented()) {
                var id = $('#id').val();
                if (save_method == 'add') url = "{{ url('siswa') }}";
                else url = "{{ url('siswa') . '/' }} " + id;

                $.ajax({
                    url : url,
                    type: "POST",
                    data : $('#modal-form form').serialize(),
                    success : function($data) {
                        $('#modal-form').modal('hide');
                        table.ajax.reload();
                    },
                    error : function(){
                        alert('Opss sesuatu salah');
                    }
                });
                return false;
               }
            });
        });

        </script>
</html>