<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;
use Yajra\Datatables\Datatables;
class SiswaController extends Controller
{
    
    public function index()
    {
        $siswa = Siswa::all();

        return view('welcome', compact('siswa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        

        return view('siswa/tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $siswa = new Siswa;
        $siswa->nisn = $request->nisn; 
        $siswa->nama = $request->nama;
        $siswa->kelas = $request->kelas;
        $siswa->save();

        return redirect('/siswa');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $siswa = Siswa::find($id);


        return view('siswa/edit', compact('siswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $siswa = Siswa::find($id);

        $siswa->update([
            'nisn' => request('nisn'),
            'nama' => request('nama'),
            'kelas' => request('kelas')
        ]);
        return redirect()->route('siswa.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $siswa = Siswa::find($id);

        $siswa->delete();

        return redirect()->route('siswa.index');
    }
    public function siswa()
    {
        $siswa = Siswa::all();
        
        return Datatables::of($siswa)
        ->addColumn('action', function($siswa){
          return '<a href="#" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-eye-open"></i> Show</a> ' .
          '<a onclick="editform('. $siswa->id_siswa .')" class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-edit"></i>edit</a>' .
          '<a onclick="editform('. $siswa->id_siswa .')" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i>Delete</a>';
        })->make(true);
    }
}
